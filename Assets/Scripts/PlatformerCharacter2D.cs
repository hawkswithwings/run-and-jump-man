using System;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class PlatformerCharacter2D : MonoBehaviour
    {
        private bool triggerEntered;
        private int dyingTimer;

        public Transform ProjectilePrefab;
        private Transform projectileTransform;
        private int crouchTimer;
        private BoxCollider2D standingBox;

        public PointsTracker tracker;
        public GameObject DeathDisplay;

        [SerializeField] private float m_MaxSpeed = 10f;                    // The fastest the player can travel in the x axis.
        [SerializeField] private float m_JumpForce = 400f;                  // Amount of force added when the player jumps.
        //[Range(0, 1)] [SerializeField] private float m_CrouchSpeed = .36f;  // Amount of maxSpeed applied to crouching movement. 1 = 100%
        [SerializeField] private bool m_AirControl = false;                 // Whether or not a player can steer while jumping;
        [SerializeField] private LayerMask m_WhatIsGround;                  // A mask determining what is ground to the character

        private Transform m_GroundCheck;    // A position marking where to check if the player is grounded.
        const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
        private bool m_Grounded;            // Whether or not the player is grounded.
        private Transform m_CeilingCheck;   // A position marking where to check for ceilings
        const float k_CeilingRadius = .01f; // Radius of the overlap circle to determine if the player can stand up
        private Animator m_Anim;            // Reference to the player's animator component.
        private Rigidbody2D m_Rigidbody2D;
        private bool m_FacingRight = true;  // For determining which way the player is currently facing.

        private Vector3 startingPos;
        
        private AudioSource jumpSound;
        private AudioSource shootSound;

        [SerializeField]private float jumpHeight;

        private void Awake()
        {
            // Setting up references.
            m_GroundCheck = transform.Find("GroundCheck");
            m_CeilingCheck = transform.Find("CeilingCheck");
            m_Anim = GetComponentInChildren<Animator>();//GetComponent<Animator>(); 
            m_Rigidbody2D = GetComponent<Rigidbody2D>();

            crouchTimer = 0;
            standingBox = gameObject.GetComponents<BoxCollider2D>()[0];

            triggerEntered = false;
            dyingTimer = 0;

            startingPos = new Vector3(this.transform.position.x, this.transform.position.y);

            jumpHeight = transform.position.y + 2.0f;

            jumpSound = gameObject.GetComponents<AudioSource>()[0];
            shootSound = gameObject.GetComponents<AudioSource>()[1];
        }


        private void FixedUpdate()
        {
            m_Grounded = false;

            // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
            // This can be done using layers instead but Sample Assets will not overwrite your project settings.
            Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject != gameObject && colliders[i].gameObject.tag != "DistanceTracker" 
                    && colliders[i].gameObject.tag != "JumpedOver")
                {
                     m_Grounded = true;
                }
               
            }
            m_Anim.SetBool("Grounded", m_Grounded);


            if (!m_Grounded && transform.position.y >= jumpHeight && m_Rigidbody2D.velocity.y > 0)
            {
                m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, 0.0f);
            }

            // Set the vertical animation
            //m_Anim.SetFloat("vSpeed", m_Rigidbody2D.velocity.y);


            if (crouchTimer > 0)
            {
                if(crouchTimer == 1)
                {
                    standingBox.enabled = true;
                    m_Anim.SetBool("Crouching", false);
                }
                crouchTimer--;
            }

            if(dyingTimer > 0)
            {
                m_Rigidbody2D.constraints = RigidbodyConstraints2D.FreezeAll;

                if (dyingTimer == 1)
                {
                    tracker.displayHighScores();
                    m_Rigidbody2D.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
                }
                dyingTimer--;
            }
        }


        public void Move(float move, bool crouch, bool jump)
        {
            // If crouching, check to see if the character can stand up
            /*if (!crouch && m_Anim.GetBool("Crouch"))
            {
                // If the character has a ceiling preventing them from standing up, keep them crouching
                if (Physics2D.OverlapCircle(m_CeilingCheck.position, k_CeilingRadius, m_WhatIsGround))
                {
                    crouch = true;
                }
            }*/

            // Set whether or not the character is crouching in the animator
            //m_Anim.SetBool("Crouch", crouch);

            //only control the player if grounded or airControl is turned on
            if (m_Grounded || m_AirControl)
            {
                // Reduce the speed if crouching by the crouchSpeed multiplier
                //move = (crouch ? move*m_CrouchSpeed : move);

                // The Speed animator parameter is set to the absolute value of the horizontal input.
                //m_Anim.SetFloat("Speed", Mathf.Abs(move));

                // Move the character
                m_Rigidbody2D.velocity = new Vector2(move*m_MaxSpeed, m_Rigidbody2D.velocity.y);

                // If the input is moving the player right and the player is facing left...
                if (move > 0 && !m_FacingRight)
                {
                    // ... flip the player.
                    Flip();
                }
                    // Otherwise if the input is moving the player left and the player is facing right...
                else if (move < 0 && m_FacingRight)
                {
                    // ... flip the player.
                    Flip();
                }
            }
            // If the player should jump...
            if (m_Grounded && jump /*&& m_Anim.GetBool("Ground")*/)
            {
                // Add a vertical force to the player.
                m_Grounded = false;
                m_Anim.SetBool("Grounded", false);
                m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
            }
        }

        public void Shoot(bool shoot)
        {
            if (shoot && crouchTimer == 0)
            {
                projectileTransform = Instantiate(ProjectilePrefab) as Transform;
                projectileTransform.position = transform.position;
            }
        }

        public void ClickJump()
        {
            if (m_Grounded && dyingTimer <= 0) //&& crouchTimer == 0)
            {
                jumpHeight = transform.position.y + 2.0f;
                //stop crouching if crouching
                if (crouchTimer > 0)
                {
                    crouchTimer = 0;
                    standingBox.enabled = true;
                    m_Anim.SetBool("Crouching", false);
                    jumpHeight *= 2.0f;
                }
                // Add a vertical force to the player.
                m_Grounded = false;
                m_Anim.SetBool("Grounded", false);
                m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
                m_Anim.Play("Jump", -1);
                jumpSound.Play();
            }
        }

        public void ClickShoot()
        {
            if (projectileTransform == null && dyingTimer <= 0)
            {
                projectileTransform = Instantiate(ProjectilePrefab) as Transform;
                projectileTransform.position = new Vector3(transform.position.x + 0.54f, transform.position.y - 0.328f);
                m_Anim.Play("Shoot", -1);
                shootSound.Play();
            }
            
        }

        public void ClickSlide()
        {
            if (crouchTimer == 0 && m_Grounded && dyingTimer <= 0) {
                standingBox.enabled = false;
                crouchTimer = 61;
                m_Anim.SetBool("Crouching", true);
                m_Anim.Play("Crouch", -1);
            }
        }

        private void Flip()
        {
            // Switch the way the player is labelled as facing.
            m_FacingRight = !m_FacingRight;

            // Multiply the player's x local scale by -1.
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }

        void OnTriggerEnter2D(Collider2D collider)
        {
            triggerEntered = true;

            if (collider.gameObject.tag == "Wall" && triggerEntered)
            {
                tracker.PauseGround();
                DeathDisplay.SetActive(true);
                Time.timeScale = 0.5f;
                m_Anim.Play("JumpManDies", -1);
                dyingTimer = 30;
            }
        }

        void OnTriggerExit2D(Collider2D collider)
        {
            if(collider.gameObject.tag == "DistanceTracker" && triggerEntered)
            {
                tracker.DistanceMarkerCrossed();
            }

            if (collider.gameObject.tag == "JumpedOver" && triggerEntered)
            {
                tracker.EnemyJumped();
            }

            if (collider.gameObject.tag == "SlidUnder" && triggerEntered)
            {
                tracker.EnemySlid();
            }

            triggerEntered = false;
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag == "Enemy")
            {
                Destroy(collision.gameObject);
                tracker.PauseGround();
                DeathDisplay.SetActive(true);
                Time.timeScale = 0.5f;
                m_Anim.Play("JumpManDies", -1);
                dyingTimer = 30;
            }
        }

        public void ResetPosition()
        {
            this.transform.position = startingPos;
        }
        
    }
}