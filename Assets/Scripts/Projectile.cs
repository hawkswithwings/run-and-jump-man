﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    private Vector2 speed;
    public GameObject owner;

    private Vector2 direction;
    private Vector2 movement;
    private bool isEnemyShot;

    public PointsTracker points;

    // Use this for initialization
    void Start ()
    {
        speed = new Vector2(7.0f, 0.0f);
        direction = new Vector2(1.0f, 0.0f);
        isEnemyShot = false;

        points = GameObject.FindGameObjectWithTag("PointsTracker").GetComponent<PointsTracker>();
    }
	
	// Update is called once per frame
	void Update () {
        
    }

    public void FixedUpdate()
	{
        movement = new Vector2(
            speed.x * direction.x,
            speed.y * direction.y);

        // Apply movement to the rigidbody
        GetComponent<Rigidbody2D>().velocity = movement;
        
        Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if ((screenPosition.x > Screen.width || screenPosition.x < 0) ||
            (screenPosition.y > Screen.height || screenPosition.y < 0))
        {
            Destroy(this.gameObject);
        }
    }

    public void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Enemy")
        {
            coll.gameObject.GetComponent<EnemyTypes>().IsShot();
            points.EnemyShot();
            Destroy(this.gameObject);
        }
    }
}
