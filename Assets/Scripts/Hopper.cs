﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hopper : EnemyTypes {

    private float startingY;
    public Rigidbody2D rigbod;
    private bool grounded;
    private bool canMove;

    private Animator anim;

    public float horizontalSpeed;
    public float jump;
    

    // Use this for initialization
    void Start () {
        startingY = this.transform.position.y;
        rigbod = this.GetComponent<Rigidbody2D>();
        grounded = true;
        canMove = true;
        jump = 8f;
        horizontalSpeed = -2f;
        anim = this.GetComponentInChildren<Animator>();
    }
	
    public void IncrementSpeed(int times)
    {
        if(times > 0)
        {
            for (int x = 0; x < times; x++)
                horizontalSpeed = horizontalSpeed - 1.0f;
        }
        print(horizontalSpeed);
    }

	// Update is called once per frame
	void Update ()
    {
        Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (screenPosition.x < 0)
        {
            Destroy(this.gameObject);
        }
    }

    void FixedUpdate()
    {
        if (grounded)
        {
            rigbod.velocity = new Vector2(horizontalSpeed, jump);
            grounded = false;
        }
        if (this.transform.position.y > startingY && canMove)
        {
            anim.Play("Jumping", -1);
            grounded = false;
        }

        /*if (this.transform.position.y <= startingY)
        {
            anim.Play("Idle", -1);
            grounded = true;
        }*/

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground" && canMove)
        {
            anim.Play("Idle", -1);
            grounded = true;
        }
    }

    public override void IsShot()
    {
        anim.Play("DeathExplosion", -1);
        horizontalSpeed = 0;
        jump = 0;
        rigbod.gravityScale = 0;
        canMove = false;
        Destroy(this.gameObject, anim.GetCurrentAnimatorClipInfo(0).Length - 0.5f);
    }

    public void OnTriggerEnter2D(Collider2D coll)
    {
        /*if (coll.gameObject.tag == "Projectile")
        {
            anim.Play("DeathExplosion", -1);
            horizontalSpeed = 0;
            jump = 0;
            rigbod.gravityScale = 0;
            canMove = false;
            Destroy(coll.gameObject);
            Destroy(this.gameObject, anim.GetCurrentAnimatorClipInfo(0).Length - 0.5f);
        }*/
    }
}
