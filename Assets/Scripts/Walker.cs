﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walker : EnemyTypes {

    public Rigidbody2D rigbod;
    public Vector2 velocity;
    private Vector2 movement;
    private Animator anim;

    private int animatedState; // 1 = Idle, 2 = Dying

    // Use this for initialization
    void Start () {
        animatedState = 1;
        rigbod = this.GetComponent<Rigidbody2D>();
        velocity = new Vector2(-8, 0);
        rigbod.velocity = velocity;
        anim = this.GetComponentInChildren<Animator>();
    }

    public void IncrementSpeed(int times)
    {
        if (times > 0)
        {
            for (int x = 0; x < times; x++)
            {
                velocity = new Vector2(velocity.x - 2, velocity.y);
            }
        }
        rigbod.velocity = velocity;
    }

	// Update is called once per frame
	void Update () {
        Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (screenPosition.x < 0)
        {
            Destroy(this.gameObject);
        }
        
    }

    void FixedUpdate()
    {
        rigbod.velocity = velocity;
    }


    public override void IsShot()
    {
        anim.Play("DeathExplosion", -1);
        velocity = new Vector2(0, 0);
        Destroy(this.gameObject, anim.GetCurrentAnimatorClipInfo(0).Length - 0.5f);
    }

    public void OnTriggerEnter2D(Collider2D coll)
    {
        /*if (coll.gameObject.tag == "Projectile")
        {
            anim.Play("DeathExplosion", -1);
            Destroy(coll.gameObject);
            velocity = new Vector2(0,0);
            Destroy(this.gameObject, anim.GetCurrentAnimatorClipInfo(0).Length - 0.5f);
        }*/
    }
}
