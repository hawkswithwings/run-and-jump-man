﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class InitializeAds : MonoBehaviour {

    private BannerView bannerView;
    //Ad ID: ca-app-pub-9565335737748248~2748074855

    // Use this for initialization
    void Start () {
		string appId = "ca-app-pub-9565335737748248~2748074855";

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);
	}
	
	// Update is called once per frame
	void Update () {
		
	}


}
