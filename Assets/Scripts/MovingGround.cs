﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingGround : MonoBehaviour {

    private Vector2 speed;
    private Vector2 direction;
    private Vector2 movement;

    private SpriteRenderer spriteImage;
    
    private new BoxCollider2D collider;

    private float heightMod;

    private Vector3 startingPos;

    public GameObject wall;

    public bool CanDisappear;
    public bool CanChangeHeight;
    
    //public Transform GroundPrefab;

    // Use this for initialization
    void Start () {
        spriteImage = GetComponentInChildren<SpriteRenderer>();

        speed = new Vector2(4.0f, 0.0f);
        direction = new Vector2(-1.0f, 0.0f);
        heightMod = 1.0f;
        
        collider = gameObject.GetComponent<BoxCollider2D>();

        startingPos = new Vector3(this.transform.position.x, this.transform.position.y);
        
    }
	
	// Update is called once per frame
	void Update () {
       
    }

    public void FixedUpdate()
    {
        

        if (transform.position.x <= -11.0f)
        {
            int chance = (int)Random.Range(1.0f, 11.0f);

            transform.position = new Vector3(12.91f, transform.position.y, transform.position.z);
            if (CanDisappear && (chance == 1 || !(spriteImage.enabled)))
            {

                collider.enabled = !(collider.enabled);
                spriteImage.enabled = !(spriteImage.enabled);
                wall.SetActive(!wall.activeSelf);
            }

            if (CanChangeHeight && (chance == 2 || heightMod < 0))
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + heightMod, transform.position.z);
                heightMod *= -1.0f;
            }
        }

        // Apply movement to the rigidbody
        GetComponent<Rigidbody2D>().velocity = movement;

        movement = new Vector2(
            speed.x * direction.x,
            speed.y * direction.y);

        
    }

    public void ResetTile()
    {
        this.transform.position = startingPos;
        collider.enabled = true;
        spriteImage.enabled = true;
        wall.SetActive(true);
        heightMod = 1.0f;
        speed = new Vector2(4.0f, 0.0f);
    }

    public void PauseGround()
    {
        speed = new Vector2(0.0f, 0.0f);
    }
}
