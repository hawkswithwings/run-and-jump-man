﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class CreateAdBanner : MonoBehaviour {

    private BannerView bannerView;

    // Use this for initialization
    void Start () {
        this.RequestBanner();
    }

    private void RequestBanner()
    {
        string adUnitId = "ca-app-pub-9565335737748248/9261867535";

        AdSize adSize = new AdSize(220, 50);

        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, adSize, AdPosition.TopLeft);
        

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        // Load the banner with the request.
        bannerView.LoadAd(request);
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void DestroyBanner()
    {
        bannerView.Destroy();
    }
}
