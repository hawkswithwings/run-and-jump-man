﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swoop : EnemyTypes {

    private float startY;
    private float endY;
    public float xSpeed;

    private Animator anim;

    public Rigidbody2D rigbod;

    // Use this for initialization
    void Start () {
        startY = this.transform.position.y;
        rigbod = this.GetComponent<Rigidbody2D>();
        endY = 2.0f;
        xSpeed = -4f;
        anim = this.GetComponentInChildren<Animator>();
    }
	
    public void IncrementSpeed(int times)
    {
        if (times > 0)
            for (int x = 0; x < times; x++)
                xSpeed = xSpeed - 2.0f;
        print("Flyby speed "+xSpeed);
    }

	// Update is called once per frame
	void Update () { 
        Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (screenPosition.x < 0)
        {
            Destroy(this.gameObject);
        }
    }

    void FixedUpdate()
    {
        if (this.transform.position.y > 0.0f)
        {
            this.transform.position = new Vector3(this.transform.position.x - 0.09f, this.transform.position.y - 0.1f, this.transform.position.z);

        }
        else if (this.transform.position.y <= 0.0f)
        {
            rigbod.velocity = new Vector2(xSpeed, 0);
        }

    }

    public override void IsShot()
    {
        anim.Play("DeathExplosion", -1);
        xSpeed = 0;
        Destroy(this.gameObject, anim.GetCurrentAnimatorClipInfo(0).Length - 0.5f);
    }

    public void OnTriggerEnter2D(Collider2D coll)
    {
        /*if (coll.gameObject.tag == "Projectile")
        {
            anim.Play("DeathExplosion", -1);
            xSpeed = 0;
            Destroy(coll.gameObject);
            Destroy(this.gameObject, anim.GetCurrentAnimatorClipInfo(0).Length - 0.5f);
        }*/
    }
}
