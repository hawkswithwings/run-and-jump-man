﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.SceneManagement;

public class PointsTracker : MonoBehaviour {

    public Text score;

    public GameObject highScoresDisplay;
    public GameObject mainOverlay;
    public Text scoresDisplayText;

    public List<MovingGround> groundTiles;

    public EnemyGenerator enemies;

    private string[] scoresDisplay;

    private int scoreLength;
    private int scoreValue;

    private List<int> HighScores;
	// Use this for initialization
	void Start () {
        if (File.Exists(Application.persistentDataPath + "/HighScores.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/HighScores.gd", FileMode.Open);
            HighScores = (List<int>)bf.Deserialize(file);
            file.Close();
        }
        else
        {
            HighScores = new List<int>()
            {
                0,0,0,0,0,0,0,0,0,0
            };
        }

        scoresDisplay = new string[]
        {
            "High Scores",
            "00000000",
            "00000000",
            "00000000",
            "00000000",
            "00000000",
            "00000000",
            "00000000",
            "00000000",
            "00000000",
            "00000000"
        };

        scoreValue = 0;
        scoreLength = 8;
        
    }

    // Update is called once per frame
    void Update()
    {

    }
	
	void UpdateDisplay () {
        string newvalue;
        newvalue = scoreValue.ToString();
        if(newvalue.Length < scoreLength)
        {
            int numZeroes = scoreLength - newvalue.Length;
            for(int i = 0; i < numZeroes; i++)
            {
                newvalue = "0" + newvalue;
            }
        }
        score.text = newvalue;
	}

    void OnApplicationQuit()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/HighScores.gd");
        bf.Serialize(file, HighScores);
        file.Close();
    }

    public int getScoreValue()
    {
        return scoreValue;
    }

    public void DistanceMarkerCrossed()
    {
        scoreValue += 50;
        UpdateDisplay();
    }

    public void EnemyShot()
    {
        scoreValue += 100;
        UpdateDisplay();
    }

    public void EnemyJumped()
    {
        scoreValue += 200;
        UpdateDisplay();
    }

    public void EnemySlid()
    {
        scoreValue += 200;
        UpdateDisplay();
    }

    public void UpdateHighScores()
    {
        int length = 10;

        for(int i = 0; i < length; i++)
        {
            if(scoreValue >= HighScores[i])
            {
                HighScores.Insert(i, scoreValue);
                HighScores.Remove(length-1);
                i = length;
            }
        }

        int index = 0;
        string scores = scoresDisplay[0] + "\n";
        for(int i = 1; i < scoresDisplay.Length; i++)
        {
            scoresDisplay[i] = HighScores[index].ToString();

            int numZeroes = scoreLength - scoresDisplay[i].Length;
            for (int j = 0; j < numZeroes; j++)
            {
                scoresDisplay[i] = "0" + scoresDisplay[i];
            }

            scores += scoresDisplay[i];
            if (i != length)
                scores += "\n";
            index++;
        }

        scoresDisplayText.text = scores;
    }

    public void displayHighScores()
    {
        Time.timeScale = 0;
        mainOverlay.SetActive(false);
        UpdateHighScores();
        highScoresDisplay.SetActive(true);
    }

    public void PlayAgain()
    {
        /*highScoresDisplay.SetActive(false);
        mainOverlay.SetActive(true);
        scoreValue = 0;
        Time.timeScale = 1;
        for(int i = 0; i < groundTiles.Count; i++)
        {
            groundTiles[i].ResetTile();
        }
        enemies.DestroyAllEnemies();
        score.text = "00000000";*/
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/HighScores.gd");
        bf.Serialize(file, HighScores);
        file.Close();
        Time.timeScale = 1;
        SceneManager.LoadScene("GameplayWithArt");
    }

    public void PauseGround()
    {
        for (int i = 0; i < groundTiles.Count; i++)
        {
            groundTiles[i].PauseGround();
        }
    }

    public void ReturnToTitle()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/HighScores.gd");
        bf.Serialize(file, HighScores);
        file.Close();
        Time.timeScale = 1;
        SceneManager.LoadScene("TitleScreen");
    }
}