﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


public class ChangeScene : MonoBehaviour {

    public string SceneToLoad;

    public GameObject ScoresDisplay;
    public Text scoreText;

    public GameObject CreditsDisplay;

    private List<int> HighScores;
    private List<string> scores;
    private string scoresDisplay;

    private bool scoresSet;
    

    // Use this for initialization
    void Start () {
        scores = new List<string>();
        
        if (File.Exists(Application.persistentDataPath + "/HighScores.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/HighScores.gd", FileMode.Open);
            HighScores = (List<int>)bf.Deserialize(file);
            file.Close();

            for (int i = 0; i < 10; i++)
            {
                string k = HighScores[i].ToString();
                int numZeroes = 8 - k.Length;
                for (int j = 0; j < numZeroes; j++)
                {
                    k = "0" + k;
                }

                scores.Add(k);
            }

            scoresDisplay = "High Scores\n" +
                scores[0] + "\n" +
                scores[1] + "\n" +
                scores[2] + "\n" +
                scores[3] + "\n" +
                scores[4] + "\n" +
                scores[5] + "\n" +
                scores[6] + "\n" +
                scores[7] + "\n" +
                scores[8] + "\n" +
                scores[9];
        }

        else
        {
            HighScores = new List<int>()
            {
                0,0,0,0,0,0,0,0,0,0
            };

            scoresDisplay = "High Scores\n"+
                "00000000\n"+
                "00000000\n" +
                "00000000\n" +
                "00000000\n" +
                "00000000\n" +
                "00000000\n" +
                "00000000\n" +
                "00000000\n" +
                "00000000\n" +
                "00000000\n";
        }

        scoresSet = false;
    }
	
	// Update is called once per frame
	void Update () {
		
	}


    public void LoadNextScene()
    {
        SceneManager.LoadScene(SceneToLoad);
    }

    public void DisplayHighScores()
    {
        if(!scoresSet)
            scoreText.text = scoresDisplay;
        ScoresDisplay.SetActive(true);
    }

    public void CloseHighScores()
    {
        ScoresDisplay.SetActive(false);
    }

    public void DisplayCredits()
    {
        CreditsDisplay.SetActive(true);
    }

    public void CloseCredits()
    {
        CreditsDisplay.SetActive(false);
    }

}
