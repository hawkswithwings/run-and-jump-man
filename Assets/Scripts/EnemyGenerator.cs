﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour {

    public PointsTracker points;

    public Transform hopperPrefab;
    public Transform swoopPrefab;
    public Transform walkerPrefab;

    private int ptsBench;
    private int ptsRemainder;
    private int IncrementAmount;

    private Transform enemy;
    private List<Transform> ActiveEnemies;

    private int spawnCountdown;
    private bool canIncrement;

	// Use this for initialization
	void Start () {
        canIncrement = false;

        ptsBench = 0;
        ptsRemainder = 1;
        IncrementAmount = 0;

        spawnCountdown = 600;
        ActiveEnemies = new List<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        if (points.getScoreValue() > 0)
        {
            ptsBench = points.getScoreValue() / 800;
            ptsRemainder = points.getScoreValue() % 800;
        }

        if(ptsRemainder == 0)
        {
            IncrementAmount = ptsBench;

            if (enemy != null && canIncrement)
            {
                if (enemy.gameObject.GetComponent<Hopper>() != null)
                {
                    enemy.gameObject.GetComponent<Hopper>().IncrementSpeed(IncrementAmount);
                }
                else if (enemy.gameObject.GetComponent<Swoop>() != null)
                {
                    enemy.gameObject.GetComponent<Swoop>().IncrementSpeed(IncrementAmount);
                }
                else if (enemy.gameObject.GetComponent<Walker>() != null)
                {
                    enemy.gameObject.GetComponent<Walker>().IncrementSpeed(IncrementAmount);
                }
                canIncrement = false;
            }

        }
        if(ActiveEnemies.Count > 0)
        {
            for(int i = 0; i < ActiveEnemies.Count; i++)
            {
                if (ActiveEnemies[i] == null)
                {
                    ActiveEnemies.RemoveAt(i);
                    spawnCountdown = 60;
                    i--;
                }
            }
        }

        if (spawnCountdown != 0)
        {
            spawnCountdown--;
        }

        if (spawnCountdown == 0 && enemy == null)
        {
            int chance = (int)Random.Range(1.0f, 4.0f);
            switch (chance)
            {
                case 1:
                    enemy = Instantiate(hopperPrefab) as Transform;
                    ActiveEnemies.Add(enemy);
                    break;
                case 2:
                    enemy = Instantiate(swoopPrefab) as Transform;
                    ActiveEnemies.Add(enemy);
                    break;
                case 3:
                    enemy = Instantiate(walkerPrefab) as Transform;
                    ActiveEnemies.Add(enemy);
                    break;
                default:
                    spawnCountdown = 300;
                    break;
            }
            canIncrement = true;
        }
	}

    

    public void DestroyAllEnemies()
    {
        for(int i = 0; i < ActiveEnemies.Count; i++)
        {
            if (ActiveEnemies[i] != null)
                Destroy(ActiveEnemies[i].gameObject);
        }

        ActiveEnemies = new List<Transform>();
        spawnCountdown = 600;
    }
}
